#!/usr/bin/env python3
""" tests for fractal_sound_explorer """

from src.fractal_sound_explorer import __version__


def test_version():
    """test version number matches expectations"""
    assert __version__ == "0.1.0"
