.. _python:

Autogen Python Docs
=====================

   This code is written in *Python 3.12*.

   It uses the docstring comments within the ``fractal_sound_explorer.py`` file to
   automatically document the Python code.

``fractal_sound_explorer.py``
--------------------------

.. automodule:: src.fractal_sound_explorer.fractal_sound_explorer
   :members:
