Standard Project Repository
========================================================

A standardised project repository layout with documentation, Gitlab CI/CD
and code testing.


.. toctree::
   :maxdepth: 2

   fractal_sound_explorer
   python_docs
